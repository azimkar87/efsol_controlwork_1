﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Repositories
{
    public interface  IRepository<T> where T:Entity
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        void Update(T entity);
        void Delete(T entity);

        void Add(T entity);


    }
}
