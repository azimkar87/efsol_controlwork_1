﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T:Entity
    {
        protected AppContext dbcontext;
        protected DbSet<T> DbSet { get; set; }

        public BaseRepository(AppContext context)
        {
            dbcontext = context;
        }

        public void Add(T entity)
        {
            DbSet.Add(entity);
            dbcontext.SaveChanges();
        }

        public void Delete(T entity)
        {
            dbcontext.Remove(entity);
            dbcontext.SaveChanges();
        }

        public T Get(int id)
        {
            return DbSet.FirstOrDefault(flat => flat.Id == id);
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }

        public void Update(T entity)
        {
            dbcontext.Update(entity);
            dbcontext.SaveChanges();
        }


    }
}
