﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Repositories
{
    public class UnitOfWork: IDisposable
    {
        private readonly AppContext db;

        private HouseRepository houseRepository;
        private FlatRepository flatRepository;

       public UnitOfWork(AppContext dbContext)
        {
            db = dbContext;
        }

        public FlatRepository Flats
        {
            get
            {
                if (flatRepository == null)
                    flatRepository = new FlatRepository(db);
                return flatRepository;
            }
        }

        public HouseRepository Houses
        {
            get
            {
                if (houseRepository == null)
                    houseRepository = new HouseRepository(db);
                return houseRepository;
            }
        }
       
        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
