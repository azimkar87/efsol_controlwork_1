﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Repositories.Interfaces;

namespace WebApplication1.Repositories
{
    public class HouseRepository : BaseRepository<House>,IHouseRepository
    {
        public HouseRepository(AppContext context):base(context)
        {
            DbSet = context.House;
        }
    }
}
