﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Flat:Entity
    {
        
        public string OwnerName { get; set; }
        public string FlatNumber { get; set; }
        public int HouseId {get;set;}

        public House House { get; set; }


    }
}
