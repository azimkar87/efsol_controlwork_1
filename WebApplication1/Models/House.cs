﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class House:Entity
    {

        public string Address { get; set; }

        public ICollection<Flat> Flats { get; set; }

        public House()
        {
            Flats = new List<Flat>();
        }

    }
}
