﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Repositories.Interfaces;

namespace WebApplication1.Controllers
{
    public class HouseController : Controller
    {
        private readonly IHouseRepository _houseRepository;
        public HouseController(IHouseRepository houseRepository)
        {
            _houseRepository = houseRepository;
        }
        public IActionResult Index()
        {
            var house = _houseRepository.GetAll();
            return View(house);
        }
    }
}