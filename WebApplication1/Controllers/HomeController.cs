﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Repositories;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        UnitOfWork unitOfWork;
        private readonly AppContext db;

        public HomeController(AppContext appContext)
        {

            db = appContext;
            unitOfWork = new UnitOfWork(db);
            
        }
        public IActionResult Index()
        {
            var flats = unitOfWork.Flats.GetAll();
            return View(flats);
        }

        public IActionResult About()
        {
            List <Flat> flats=unitOfWork.Flats.GetAll().ToList();
            return View(flats);
        }
 

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
